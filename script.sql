create database aplicacion;
use aplicacion;


create table pabellon(
id int primary key auto_increment,
nombre varchar(40) unique not null,
localidad_pabellon varchar(40),
fecha_apertura date
);


create table equipo(
id int primary key auto_increment,
nombre varchar(40),
id_pabellon int unique,
localidad_equipo varchar(40),
foreign key (id_pabellon) references pabellon (id)
);

create table jugador(
id int primary key auto_increment,
nombre varchar(40),
dorsal int,
fecha_nacimiento date,
id_equipo int,
foreign key (id_equipo) references equipo (id)
);

delimiter $$
create procedure alta_equipo(pnombre varchar(40),pid_pabellon int, plocalidad_equipo varchar(40))
begin
if((select count(*) from equipo where id_pabellon=pid_pabellon))>0 then
select "equipo con ese pabellon ya creado";
else
insert into equipo(nombre,id_pabellon,localidad_equipo) values (pnombre,pid_pabellon,plocalidad_equipo);
end if;
end;$$


delimiter $$
create procedure borrar_equipo(pid_equipo int)
begin
if((select count(*) from jugador where id_equipo=pid_equipo))=0 then
delete from equipo where id = pid_equipo;
else
update jugador set id_equipo= null where id_equipo = pid_equipo;
delete from equipo where id = pid_equipo;
end if;
end;$$



delimiter $$
create procedure mostrar_jugador()
begin
if(select id from jugador where id_equipo = null) then
select * from jugador;
else
select jugador.id,jugador.nombre,jugador.dorsal,jugador.fecha_nacimiento,equipo.nombre as 'Nombre Equipo' FROM jugador, equipo where equipo.id = jugador.id_equipo;
end if;
end;$$


delimiter $$
create procedure alta_jugador(pnombre varchar(40),pdorsal varchar(40), pfecha_nacimiento date, pid_equipo int)
begin
if((select count(*) from jugador where id_equipo=pid_equipo and nombre = pnombre))>0 then
select "jugador con ese equipo ya creado";
else
insert into jugador(nombre,dorsal,fecha_nacimiento,id_equipo) values (pnombre,pdorsal,pfecha_nacimiento,pid_equipo);
end if;
end;$$

delimiter $$
create procedure borrar_pabellon(pid_pabellon int)
begin
if((select count(*) from equipo where id_pabellon=pid_pabellon))=0 then
delete from pabellon where id = pid_pabellon;
else
update equipo set id_pabellon= null where id_pabellon = pid_pabellon;
delete from pabellon where id = pid_pabellon;
end if;
end;$$


delimiter $$
create procedure mostrar_campos(caracteres varchar(40))
begin
declare cadena varchar(40);
set cadena = caracteres;
if((select count(id) from pabellon where nombre like cadena))>=1 then
select * from pabellon where nombre like cadena;
else if((select count(id) from equipo where nombre like cadena))>=1 then
select * from equipo where nombre like cadena;
else if((select count(id) from jugador where nombre like cadena))>=1 then
select * from jugador where nombre like cadena;
end if;
end if;
end if;
end;$$

delimiter $$
create function contar_jugadores_equipo(pid int)
returns int
begin
return (select count(id) from jugador where id_equipo=pid);
end;$$

delimiter $$
create function primer_pabellon_abierto()
returns varchar(50)
begin
return (select concat(id,' ', nombre,' ',localidad_pabellon,' ',fecha_apertura) from pabellon where fecha_apertura=(select min(fecha_apertura) from pabellon));
end;$$

