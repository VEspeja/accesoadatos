package MVC;

import com.github.lgooddatepicker.components.DatePicker;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDate;

/**
 * la clase Vista hereda de JFrame, adquiriendo sus propiedades a modificar
 */
public class Vista extends JFrame{
    JTabbedPane tabbedPane1;
    JPanel panel1;
    private JPanel Equipo;
    private JPanel Jugador;
    private JPanel Pabellon;
    JTextField txtNombreEquipo;
    JTextField txtLocalidad;
    private JLabel etNombre;
    JComboBox cbPabellon;
    JComboBox cbEquipo;
    private JLabel etPabellon;
    private JLabel etLocalidad;
    JTextField txtNombreJugador;
    JTextField txtDorsal;
    JButton btnAltaJugador;
    JButton btnEliminarJugador;
    JButton btnModificarJugador;


     JTextField txtNombrePabellon;
     JTextField txtLocalidadPabellon;

     JButton btnAltaEquipo;
     JButton btnEliminarEquipo;
     JButton btnModificarEquipo;
     JButton btnAltaPabellon;
     JButton btnEliminarPabellon;
     JButton btnModificarPabellon;

     DatePicker dtFechaApertura;
     DatePicker dpFechaNacimiento;

    private JTable JTablePabellon;
    private JTable JTableJugador;
    private JTable JTableEquipo;
    JTextField txtOperarJugador;
    JTextField txtOperarPabellon;
    JTextField txtOperarEquipo;
    private JPanel Busqueda;
     JTextField txtBusqueda;
     JTable JTableBusqueda;
     JButton btnBuscar;
     JPanel Configuracion;
     JTextField txtHost;
     JTextField txtBaseDatos;
     JTextField txtUser;
     JTextField txtPassword;
    JTextField txtPort;
     JRadioButton rbMy;
     JRadioButton rbPostgre;
    JTextField txtCantidadJugadores;
    JButton btnConsultar;
    JButton btnCrearTablas;
    JTextField txtUltimoPabellon;
    JButton btnConsultarPabellon;
    JButton btnCambiarProperties;


    VistaLogin vistaLogin;

    JMenuItem itemExportar;


    // DCM de tipo Pabellon que se usara en el ComboBox de Pabellones en la pestanya de Equipo
    DefaultComboBoxModel<String> dcbmPabellones;
    //DCM de tipo Jugador que se usara en el ComboBox de Jugadores a anyadir en la pestanya Equipo
    DefaultComboBoxModel<String> dcbmEquipos;
    public DefaultTableModel dtmPabellon;
    public DefaultTableModel dtmJugador;
    public DefaultTableModel dtmEquipo;
    public DefaultTableModel dtmBusqueda;


    /**
     * se declara un constructor que recibe una VistaLogin que sera inicializada como un Jdialog para el login
     * tambien se llamara al metodo init() que se encarga de preparar el contenido de la clase
     * @param unaVistaLogin es un Obejeto de la clase VistaLogin
     */

    // tambien inicializaremos los elementos a traves de varios metodos privados
    public Vista(VistaLogin unaVistaLogin){
        // asigno a el objeto vistaLogin el objeto que recibe
        vistaLogin=unaVistaLogin;
        // declaro el JDialog de la clase VistaLogin, haciendo que esta clase sea la padre, y que sea modal
       vistaLogin.vistaLogin = new JDialog(this,"ventana login",true);
        // el metodo init se engarga de dar las propiedades de la ventana
        init();
        JTablePabellon.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                JTable table = (JTable) e.getSource();
                Point point = e.getPoint();
                int row = table.rowAtPoint(point);
                if(e.getClickCount() == 1){
                    txtOperarPabellon.setText(JTablePabellon.getValueAt(JTablePabellon.getSelectedRow(),0).toString());
                    txtNombrePabellon.setText(JTablePabellon.getValueAt(JTablePabellon.getSelectedRow(),1).toString());
                    txtLocalidadPabellon.setText(JTablePabellon.getValueAt(JTablePabellon.getSelectedRow(),2).toString());
                    String fecha = null;
                    try{
                         fecha = JTablePabellon.getValueAt(JTablePabellon.getSelectedRow(),3).toString();
                    }catch (NullPointerException ne){
                        System.out.println("hola");
                    }
                    if (fecha!=null) {
                        dtFechaApertura.setDate(LocalDate.parse(fecha));
                    }

                }

            }
        });
        JTableJugador.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                JTable table = (JTable) e.getSource();
                Point point = e.getPoint();
                String equipo = null;
                int row = table.rowAtPoint(point);
                if(e.getClickCount() == 1){
                    txtOperarJugador.setText(JTableJugador.getValueAt(JTableJugador.getSelectedRow(),0).toString());
                    txtNombreJugador.setText(JTableJugador.getValueAt(JTableJugador.getSelectedRow(),1).toString());
                    txtDorsal.setText(JTableJugador.getValueAt(JTableJugador.getSelectedRow(),2).toString());
                    dpFechaNacimiento.setDate(LocalDate.parse(JTableJugador.getValueAt(JTableJugador.getSelectedRow(),3).toString()));
                    if(JTableJugador.getValueAt(JTableJugador.getSelectedRow(),4)!=null){
                        equipo = JTableJugador.getValueAt(JTableJugador.getSelectedRow(),4).toString();
                        for (int i = 0; i < dcbmEquipos.getSize(); i++){
                            String cadena = dcbmEquipos.getElementAt(i);
                            String id[] = cadena.split(" ");
                            if(id[0].equalsIgnoreCase(equipo)){
                                dcbmEquipos.setSelectedItem(dcbmEquipos.getElementAt(i));
                            }
                        }

                    }
                    else{
                        equipo = "Esta libre";
                        dcbmEquipos.setSelectedItem(equipo);
                    }

                }
            }
        });

        JTableEquipo.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                JTable table = (JTable) e.getSource();
                Point point = e.getPoint();
                int row = table.rowAtPoint(point);
                String pabellon = null;
                if(e.getClickCount() == 1){
                    txtOperarEquipo.setText(JTableEquipo.getValueAt(JTableEquipo.getSelectedRow(),0).toString());
                    txtNombreEquipo.setText(JTableEquipo.getValueAt(JTableEquipo.getSelectedRow(),1).toString());
                    System.out.println(JTableEquipo.getValueAt(JTableEquipo.getSelectedRow(),2));
                    if(JTableEquipo.getValueAt(JTableEquipo.getSelectedRow(),2)!=null){
                        pabellon = JTableEquipo.getValueAt(JTableEquipo.getSelectedRow(),2).toString();
                        for (int i = 0; i < dcbmPabellones.getSize(); i++) {
                            String cadena = dcbmPabellones.getElementAt(i);
                            String id[] = cadena.split(" ");
                            if (id[0].equalsIgnoreCase(pabellon)) {
                                dcbmPabellones.setSelectedItem(dcbmPabellones.getElementAt(i));
                            }
                        }
                    }
                    else{
                        pabellon ="no posee pabellon";
                        dcbmPabellones.setSelectedItem(pabellon);
                    }
                    //dcbmPabellones.setSelectedItem(JTableEquipo.getValueAt(JTableEquipo.getSelectedRow(),2));
                    txtLocalidad.setText(JTableEquipo.getValueAt(JTableEquipo.getSelectedRow(),3).toString());




                }
            }
        });
    }

    /**
     * el metodo init se encarga de dar las propiedades necesarias para la correcta ejecuccion de
     * la ventana, tanto el titulo como tamanyo o la colocacion en la ventana entre otras
     * y de llamar a los metodos iniciarModelos() y crearMenu()
     */
    private void init(){
            // anyadimos un titulo a la ventana
            setTitle("Equipos");
            setContentPane(panel1);
            // damos dimensiones a la ventana
            setSize(700, 400);
            // la hacemos visible
            setVisible(true);
            // le asignamos la caracteristica de cuando se cierre finaliza el programa
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            // colocamos la ventana en el centro de la pantalla
            setLocationRelativeTo(null);
            // le anyadimos al panel principal un borderLayout
            getContentPane().setLayout(new BorderLayout(0, 0));
            // este metodo se encarga de que se inicialicen los Default ... Model que vamos a usar
            iniciarModelos();
            // el metodo crear menu se encarga de construir los elementos y colocarlos en la ventana
            crearMenu();

    }

    /**
     * el metodo crearMenu se encarga de inicializar los elementos y darles un ActionComand
     * ademas de crear los elemetos en los que iran alojados como una barra y el menu desplegable
     * en el que seran anyadidos
     */
    private void crearMenu(){
        // declaro una barra en la que anyadiremos los elementos
        JMenuBar barra = new JMenuBar();
        // creo un Menu en el que se anyadira los temas relaciondos con archivos
        JMenu menu = new JMenu("Archivo");
        JMenu menu2 = new JMenu("Conexion");

        itemExportar = new JMenuItem("Exportar a XML");
        // añado los actions comands a los botones
        itemExportar.setActionCommand("Exportar");


        // se anyaden los MenuItem a los 2 menus que hemos creado
        menu.add(itemExportar);

        // se anyaden los menus a la barra
        barra.add(menu);

        // anyado la barra con los elementos a la ventana
        setJMenuBar(barra);
    }

    /**
     * este metodo se encarga de inicializar los modelos que usaremos tanto los DefaultListModel
     * como los DefaultComboBoxModel y se asignaran a sus respectivos elementos
     */
    private void iniciarModelos() {
        // declaramos los default comoboBox Model
        dcbmEquipos = new DefaultComboBoxModel<>();
        dcbmPabellones = new DefaultComboBoxModel<>();
        dtmPabellon = new DefaultTableModel();
        JTablePabellon.setModel(dtmPabellon);
        dtmJugador = new DefaultTableModel();
        JTableJugador.setModel(dtmJugador);
        dtmEquipo= new DefaultTableModel();
        JTableEquipo.setModel(dtmEquipo);
        dtmBusqueda = new DefaultTableModel();
        JTableBusqueda.setModel(dtmBusqueda);
        // asignamos a los ComboBox los DCM
        cbPabellon.setModel(dcbmPabellones);
        cbEquipo.setModel(dcbmEquipos);

    }

}
