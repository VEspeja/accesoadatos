package MVC;

import principal.Util;
import usuarios.Usuario;

import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Properties;

public class Controlador implements ActionListener, KeyListener, WindowListener {
    // creo un objeto de tipo Vista
    private Vista vista ;
    // creo un objeto de tipo Modelo
    private Modelo modelo;
    // creo un objeto de tipo VistaLogin
    private VistaLogin vistaLogin;
    private Util util;
    boolean contrasena;
    boolean usuario;
    boolean anyadido;
    boolean conectado;
    ResultSet resultado;

        /**
         * el constructor se encarga de inicializar los objetos privados
         * y de llamar a los metodos que anyaden los listeners ademas de cargar
         * la informacion de la aplicacion y refrescar los elementos de la ventana
         * @param unaVista de objeto Vista
         * @param unModelo de objeto Modelo
         * @param vistaLogin de objeto VistaLogin
         * @param unUtil de objeto Util
         */
    public Controlador(Vista unaVista, Modelo unModelo, VistaLogin vistaLogin, Util unUtil){
        // declaro la vista con la Vista que recibo como parametro
        this.vista = unaVista;
        vista.setVisible(false);
        this.vistaLogin = vistaLogin;
        // declaro el modelo con el Modelo que recibo como parametro
        this.modelo = unModelo;
        // declaro el Util con el util que recibo como parametro
        this.util = unUtil;
        // llamo al metodo que se encarga de anyadir los ActionListeners necesarios
        anyadirActionListener(this);
        // llamo al metodo que anyade los WindowListener que voy a usar
        anadirWindowListener(this);
        // llamo al metodo que anyade los KeyListener que vou a usar
        anadirKeyListeners(this);
        cargarFichero();
        conectado = modelo.conectar();
        if(conectado ==true){
            mostrarPabellones();
            mostrarJugadores();
            mostrarEquipos();
            listarPabellones();
            listarEquipos();
            Actualizar actualizar = new Actualizar();
            actualizar.start();
        }
    }

    private void cargarFichero() {
        Properties configuracion = new Properties();
        try {
            configuracion.load(new FileInputStream("configuracionBBDD.props"));
            String driver = configuracion.getProperty("driver");
            if (driver.equals("com.mysql.jdbc.Driver"))
                vista.rbMy.setSelected(true);
            else
                vista.rbPostgre.setSelected(true);

            vista.txtHost.setText(configuracion.getProperty("servidor"));
            vista.txtPort.setText(configuracion.getProperty("puerto"));
            vista.txtBaseDatos.setText(configuracion.getProperty("basedatos"));
            vista.txtUser.setText(configuracion.getProperty("usuario"));
            vista.txtPassword.setText(configuracion.getProperty("contrasena"));

        } catch (FileNotFoundException e) {

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * este metodo se encarga de anyadir a los botones e items los ActionListener
     * @param listener de tipo ActionListener
     */
    private void anyadirActionListener(ActionListener listener) {
        vista.btnAltaEquipo.addActionListener(listener);
        vista.btnEliminarEquipo.addActionListener(listener);
        vista.btnModificarEquipo.addActionListener(listener);
        vista.btnAltaJugador.addActionListener(listener);
        vista.btnModificarJugador.addActionListener(listener);
        vista.btnEliminarJugador.addActionListener(listener);
        vista.btnAltaPabellon.addActionListener(listener);
        vista.btnModificarPabellon.addActionListener(listener);
        vista.btnEliminarPabellon.addActionListener(listener);
        vista.btnConsultar.addActionListener(listener);
        vista.itemExportar.addActionListener(listener);
        vista.btnCrearTablas.addActionListener(listener);
        //vista.btnEliminar.addActionListener(listener);
        vistaLogin.btnLogin.addActionListener(listener);
        vistaLogin.btnRegistrar.addActionListener(listener);
        vistaLogin.btnSalir.addActionListener(listener);
        vista.btnCambiarProperties.addActionListener(listener);
        vista.btnConsultarPabellon.addActionListener(listener);


    }

    /**
     * este metodo se encarga de anyadir a los campos el KeyListener
     * @param listener de tipo KeyListener
     */
    private void anadirKeyListeners(KeyListener listener) {
        vista.txtBusqueda.addKeyListener(listener);
    }

    /**
     * este metodo se encarga de anyadir a la ventana principal el WindowListener
     * @param listener de tipo WindowListener
     */
    // este metodo se encarga de anyadir a la ventana el windowListener
    private void anadirWindowListener(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    /**
     * Este metodo se encarga de reconocer que accion se produce al pulsar un elemento
     * @param e evento ACTION
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // recojo en una variable de tipo String el comando que ha sido usado al pulsar un elemento
        String comando = e.getActionCommand();
        anyadido = false;
        // se crea un switch sobre ese variable de tipo String
        switch (comando){

            // el caso de AltaEquipo se encarga de realizar el alta de un objeto de tipo Equipo a traves de un metodo disenyado en el modelo
            case "AltaEquipo":
                String cadena = String.valueOf(vista.dcbmPabellones.getSelectedItem());
                String id[] = cadena.split(" ");
                System.out.println(id[0]);
                try {
                    modelo.insertarEquipo(vista.txtNombreEquipo.getText(),Integer.parseInt(id[0]),vista.txtLocalidad.getText());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                borrarDatosEquipo();

                break;
            // el caso ModificarEquipo se encarga de realizar una modificacion del equipo que ha sido seleccionado en la lista de equipos
            case "ModificarEquipo":
                String cadena1 = String.valueOf(vista.dcbmPabellones.getSelectedItem());
                String id1[] = cadena1.split(" ");
                //  se accede al modelo y llamamos al metodo modificarEquipo al cual le pasamos como parametros los nuevos cambios que ha habido en los elementos de la pestanya Equipo,
                // tambien se le pasa el equipo que ha sido seleccionado para modificar
                try {
                    modelo.modificarEquipo(Integer.parseInt(vista.txtOperarEquipo.getText()),vista.txtNombreEquipo.getText(),Integer.parseInt(id1[0]),vista.txtLocalidad.getText());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                // una vez modificado se llama al metodo borrarDatosEquipo para dejar los elementos de la pestanya Equipo vacios
                borrarDatosEquipo();

                break;
            // el caso EliminarEquipo se encarga de eliminar de la lista el equipo que ha sido seleccionado
            case "EliminarEquipo":
                // accedemos al dlmEquipo que se encuentra en la vista, y eliminamos de ahi el equipo que hemos selecionado, se coge el equipoSeleccionado de el metodo valueChanged
                try {
                    modelo.eliminarEquipo(Integer.parseInt(vista.txtOperarEquipo.getText()));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                // una vez borrado el equipo de la lista llamamos al metodo borrarDatosEquipo para dejar los campos de la pestanya Equipo vacios
                borrarDatosEquipo();
                break;
            case "AltaJugador":
                String cadena2 = String.valueOf(vista.dcbmEquipos.getSelectedItem());
                String id2[] = cadena2.split(" ");
                System.out.println(id2[0]);
                try {
                    modelo.insertarJugador(vista.txtNombreJugador.getText(),Integer.parseInt(vista.txtDorsal.getText()),vista.dpFechaNacimiento.getDate(), Integer.parseInt(id2[0]));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                break;
            case "ModificarJugador":
                String cadena3 = String.valueOf(vista.dcbmEquipos.getSelectedItem());
                String id3[] = cadena3.split(" ");
                try {
                    modelo.modificarJugador(Integer.parseInt(vista.txtOperarJugador.getText()),vista.txtNombreJugador.getText(),Integer.parseInt(vista.txtDorsal.getText()),vista.dpFechaNacimiento.getDate(),Integer.parseInt(id3[0]));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                borrarDatosJugador();
                break;

            case "EliminarJugador":
                try {
                    modelo.eliminarJugador(Integer.parseInt(vista.txtOperarJugador.getText()));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                borrarDatosJugador();
                break;
                // se anyade un pabellon
            case "AltaPabellon":
                try {
                    modelo.insertarPabellon(vista.txtNombrePabellon.getText(),vista.txtLocalidadPabellon.getText(),vista.dtFechaApertura.getDate());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
                // sirve para modificar un pabellon seleccionado
            case "ModificarPabellon":

                try {
                    System.out.println(vista.dtFechaApertura.getDate());
                    modelo.modificarPabellon(Integer.parseInt(vista.txtOperarPabellon.getText()),vista.txtNombrePabellon.getText(), vista.txtLocalidadPabellon.getText(),vista.dtFechaApertura.getDate());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                borrarDatosPabellon();
                break;
                // sirve para eliminar un pabellon seleccionado
            case "EliminarPabellon":
                try {
                    modelo.eliminarPabellon(Integer.parseInt(vista.txtOperarPabellon.getText()));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                borrarDatosPabellon();

                break;

                // sirve para exportar la informacion de la aplicacion a un archivo xml
            case "Exportar":
                System.out.println("Holaaaaaa exportado");
                try {
                    modelo.exportarPabellonXML();
                    modelo.exportarEquipoXML();
                    modelo.exportarJugadorXML();

                } catch (ParserConfigurationException e1) {
                    e1.printStackTrace();
                } catch (TransformerException e1) {
                    e1.printStackTrace();
                }


                break;

            // el case Login se encarga de el acceso de los usuarios, a traves del JDialog del login
            case "Login":
                // si uno de los campos o ambos campos de la vistaLogin estan vacios salta una ventana emergente de error
                if(vistaLogin.txtContrasenya.getText().equalsIgnoreCase("") || vistaLogin.txtUsuario.getText().equalsIgnoreCase("")){
                    // se llama a la vista, accedemos al util y llamamos al mensaje de faltan datos,
                    // pasandole el texto que queremos mostrar en la ventana
                    vistaLogin.util.mensajeLoginFaltaDatos("Campos no completos, ingrese datos");
                }
                // si estan rellenos, pasamos a buscar el contenido
                else {
                    System.out.println(vistaLogin.listaUsuarios.getLista().size());
                    // realizamos un for segun el tamanyo que tiene la lista de usuarios que hay en la vistaLogin
                    for(int i = 0 ; i<vistaLogin.listaUsuarios.getLista().size(); i++ ) {
                        // si el texto que recogemos del caompo nombre de la vista coincide con un nombre
                        // de los que tiene un usuario de la lista
                        // hacemos que el boolean de usuario sea true;
                        if(vistaLogin.txtUsuario.getText().equalsIgnoreCase(vistaLogin.listaUsuarios.getLista().get(i).getUser())) {
                            // ponemos el boolean usuario en true
                            usuario = true;
                            // hacemos un if para comprobar si la contrasenya que recibimos del campo contrasenya de la vista
                            // es igual a la contrasenya del usuario que coincide con el nombre
                            if(vistaLogin.txtContrasenya.getText().equalsIgnoreCase(vistaLogin.listaUsuarios.getLista().get(i).getContrasena())) {
                               // si coincide hacemos que el boolean contrasenya sea true
                                usuario=true;
                                contrasena=true;
                                // hacemos que desaparezca la vista de login
                                vistaLogin.dispose();
                                if(!vistaLogin.listaUsuarios.getLista().get(i).isAdmin()){
                                    vista.tabbedPane1.setEnabledAt(vista.tabbedPane1.indexOfComponent(vista.Configuracion),false);
                                }
                                // y hacemos visible la vista principal de la aplicacion
                                vista.setVisible(true);

                            }
                            else {
                                if(!contrasena) {
                                    vistaLogin.util.mensajeLoginError("Contraseña erronea");
                                }
                            }
                        }
                    }
                    if(!usuario){
                        vistaLogin.util.mensajeLoginError("El usuario no es correcto");
                    }


                }

                break;
                // se encarga de dar un nuevo usuario a la aplicacion para acceder
            case "registrar":
                // se comprueba si hay contenido en los campos de login
                if(vistaLogin.txtContrasenya.getText().equalsIgnoreCase("") || vistaLogin.txtUsuario.getText().equalsIgnoreCase("")){
                    vistaLogin.util.mensajeLoginFaltaDatos("Campos no completos, ingrese datos");
                }
                else {
                    // se comprueba si el checkBox de admin esta seleccionado
                    if (vistaLogin.cbAdmin.isSelected()) {
                        // se crea un usuario admin
                        Usuario unUsuarioAdmin = new Usuario(vistaLogin.txtUsuario.getText(), vistaLogin.txtContrasenya.getText(), true);
                        vistaLogin.listaUsuarios.getLista().add(unUsuarioAdmin);
                        System.out.println(vistaLogin.listaUsuarios.getLista().size());
                    }
                    // si no lo esta se crea un usuario normal
                    else {
                        Usuario unUsuarioNormal1 = new Usuario(vistaLogin.txtUsuario.getText(), vistaLogin.txtContrasenya.getText(), false);
                        vistaLogin.listaUsuarios.getLista().add(unUsuarioNormal1);
                        for (int i = 0; i < vistaLogin.listaUsuarios.getLista().size(); i++) {
                            System.out.println(vistaLogin.listaUsuarios.getLista().get(i).toString());
                        }
                    }
                }
                break;
                // sirve para cerrar la aplicacion en caso de no querer logearse
            case "salir":
                System.exit(0);
                break;

            case "Consultar":
                try {
                    modelo.contarJugadoresEquipo(Integer.parseInt(vista.txtOperarJugador.getText()));

                    vista.txtCantidadJugadores.setText(modelo.contarJugadoresEquipo(Integer.parseInt(vista.txtOperarJugador.getText())));

                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "CrearBBDD":
                modelo.conectarSinBaseDatos();
                try {
                    modelo.crearBaseDatos();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                modelo.conectarNuevaBase();
                try {
                    modelo.crearTablas();
                    modelo.crearProcedimientos();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                modelo.conectar();
                conectado = true;
                if(conectado == true){
                    Actualizar actualizar = new Actualizar();
                    actualizar.start();
                }
                break;
            case "Cambiar":
                crearProperties();
                break;
            case "ConsultarUltimoPabellon":
                try {
                    vista.txtUltimoPabellon.setText(modelo.consultarUltimoPabellon());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

        }
        if(conectado == true){
            // se refrescan los elementos de la lista
            mostrarPabellones();
            mostrarJugadores();
            mostrarEquipos();
            listarPabellones();
            listarEquipos();
        }

    }
    private void crearProperties() {
        Properties configuracion = new Properties();
        if (vista.rbMy.isSelected()) {
            configuracion.setProperty("driver", "com.mysql.jdbc.Driver");
            configuracion.setProperty("protocolo", "jdbc:mysql://");
        }
        else {
            configuracion.setProperty("driver", "org.postgresql.Driver");
            configuracion.setProperty("protocolo", "jdbc:postgresql://");
        }
        configuracion.setProperty("servidor",vista.txtHost.getText());
        configuracion.setProperty("basedatos", vista.txtBaseDatos.getText());
        configuracion.setProperty("puerto", vista.txtPort.getText());
        configuracion.setProperty("usuario", vista.txtUser.getText());
        configuracion.setProperty("contrasena", vista.txtPassword.getText());

        try {
            configuracion.store(new FileOutputStream("configuracionBBDD.props"), "Fichero de propiedades");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private void mostrarEquipos() {
        vista.dtmEquipo.setRowCount(0);
        vista.dtmEquipo.setColumnCount(0);
        try {
            resultado = modelo.consultarEquipos();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        //Obteniendo la informacion de las columnas que estan siendo consultadas
        listar(vista.dtmEquipo);
    }

    private void mostrarJugadores() {
        vista.dtmJugador.setRowCount(0);
        vista.dtmJugador.setColumnCount(0);
        try {
            resultado = modelo.consultarJugadores();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        //Obteniendo la informacion de las columnas que estan siendo consultadas
        listar(vista.dtmJugador);
    }
    private void mostrarPabellones() {
        vista.dtmPabellon.setRowCount(0);
        vista.dtmPabellon.setColumnCount(0);
        try {
            resultado = modelo.consultarPabellones();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        //Obteniendo la informacion de las columnas que estan siendo consultadas
        listar(vista.dtmPabellon);
    }

    private void listar(DefaultTableModel dtm) {
        ResultSetMetaData rsMd = null;
        try {
            rsMd = resultado.getMetaData();
            int cantidadColumnas = 0;
            cantidadColumnas = rsMd.getColumnCount();
            //Establecer como cabezeras el nombre de las colimnas
            for (int i = 1; i <= cantidadColumnas; i++) {
                dtm.addColumn(rsMd.getColumnLabel(i));
            }
            //Creando las filas para el JTable
            while (resultado.next()) {
                Object[] fila = new Object[cantidadColumnas];
                for (int i = 0; i < cantidadColumnas; i++) {
                    fila[i]=resultado.getObject(i+1);
                }
                dtm.addRow(fila);
            }
            resultado.close();

        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    private void listarEquipos() {
        vista.dcbmEquipos.removeAllElements();
        //vista.dcbmPabellones.addElement(null);
        try {
            resultado = modelo.consultarEquipos();
            while (resultado.next()) {
                String idEquipo = resultado.getString("id");
                String nombre = resultado.getString("nombre");
                vista.dcbmEquipos.addElement(idEquipo+" - "+nombre);
            }
            resultado.close();

        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }
    private void listarPabellones() {
        vista.dcbmPabellones.removeAllElements();
        try {
            resultado = modelo.consultarPabellones();
            while (resultado.next()) {
                String pabellon = resultado.getString("id");
                String nombre = resultado.getString("nombre");
                vista.dcbmPabellones.addElement(pabellon+" - "+nombre);

            }
            resultado.close();

        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }


    /**
     * este metodo sirve vaciar el contenido
     * de los elementos que hay en la pestanya Pabellon
     */
    private void borrarDatosPabellon() {
        vista.txtNombrePabellon.setText("");
        vista.txtLocalidadPabellon.setText("");
        vista.dtFechaApertura.clear();
        vista.txtOperarPabellon.setText("");
    }

    /**
     * este metodo sirve vaciar el contenido
     * de los elementos que hay en la pestanya Jugador
     */
    private void borrarDatosJugador() {
        vista.txtNombreJugador.setText("");
        vista.txtDorsal.setText("");
        vista.cbEquipo.setSelectedIndex(0);
        vista.dpFechaNacimiento.clear();
    }

    /**
     * este metodo sirve vaciar el contenido de
     * los elementos que hay en la pestanya Equipo
     */
    private void borrarDatosEquipo() {
        vista.txtNombreEquipo.setText("");
        vista.cbPabellon.setSelectedIndex(0);
        vista.txtLocalidad.setText("");

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * se encarga de comparar el evento recibido con la tecla pulsada en un elemento
     * de la ventana
     * @param e de tipo Key
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBusqueda) {
            if (vista.txtBusqueda.getText().length() > 1) {
                vista.dtmBusqueda.setRowCount(0);
                vista.dtmBusqueda.setColumnCount(0);
                try {
                    resultado = modelo.mostrarBusqueda(vista.txtBusqueda.getText());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                listar(vista.dtmBusqueda);
            }
            else{
                vista.dtmBusqueda.setRowCount(0);
                vista.dtmBusqueda.setColumnCount(0);
            }
        }

    }
    @Override
    public void windowOpened(WindowEvent e) {

    }
    @Override
    public void windowClosing(WindowEvent e) {

    }
    @Override
    public void windowClosed(WindowEvent e) {
        conectado = false;
    }
    @Override
    public void windowIconified(WindowEvent e) {

    }
    @Override
    public void windowDeiconified(WindowEvent e) {

    }
    @Override
    public void windowActivated(WindowEvent e) {

    }
    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    private class Actualizar extends Thread{

        @Override
        public void run() {

                    while (true) {
                        try {
                            sleep(45000);
                            mostrarPabellones();
                            System.out.println("comprobacion realizada correctamente");

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
        }
    }
}

