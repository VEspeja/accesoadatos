package MVC;


import principal.Util;
import usuarios.ListaUsuarios;

import javax.swing.*;

public class VistaLogin extends JDialog {
    public JDialog vistaLogin;
    private JPanel contentPane;
    public ListaUsuarios listaUsuarios;
    public Util util;
    public JButton btnLogin;
    public JButton btnRegistrar;
    public JTextField txtUsuario;
    public JTextField txtContrasenya;
    public JCheckBox cbAdmin;
    public JButton btnSalir;


    /**
     * se crea un constructor que va a recibir una lista de usuarios y un util, ademas de asignar
     * los ActionComands a los botones que aparecen en la clase, y de hacer visible la ventana
     * @param unaListaUsuarios es un objeto de tipo ListaUsuarios
     * @param util es un objeto de tipo Util
     */
    public VistaLogin(ListaUsuarios unaListaUsuarios, Util util) {

        this.listaUsuarios = unaListaUsuarios;
        this.util = util;
        btnLogin.setActionCommand("Login");
        btnRegistrar.setActionCommand("registrar");
        btnSalir.setActionCommand("salir");
        setContentPane(contentPane);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        //setModal(true);
        getRootPane().setDefaultButton(btnLogin);
        pack();
        setVisible(true);

    }

}
