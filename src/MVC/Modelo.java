package MVC;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {
    private File ficheroPorDefecto;
    //private File ficheroConfiguracion;
    private Properties configuracion;
    private String ruta;
    private Connection conexion;
    private String baseDatos;
    private String puerto;
    private String servidor;
    private String protocolo;
    private String driver;
    private String usuario;
    private String contrasena;

    /**
     * el constructor del modelo se encarga de inicializar los arrayLists
     * y el Properties para su fichero
     */
    public Modelo() {
        configuracion = new Properties();
        //ficheroConfiguracion = new File("configuracion.conf");

    }

    public boolean conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/aplicacion",
                    "root", "");
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se ha podido conectar con la Base de Datos");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }


    public void conectarSinBaseDatos(){
        System.out.println("holaaaaaaaaaaaaa");
        try {
            configuracion.load(new FileInputStream("configuracionBBDD.props"));

            driver = configuracion.getProperty("driver");
            protocolo = configuracion.getProperty("protocolo");
            servidor = configuracion.getProperty("servidor");
            puerto = configuracion.getProperty("puerto");
            baseDatos = configuracion.getProperty("basedatos");
            usuario = configuracion.getProperty("usuario");
            contrasena = configuracion.getProperty("contrasena");

            Class.forName(driver).newInstance();
            conexion = DriverManager.getConnection(protocolo + servidor + ":" + puerto,usuario,contrasena);
            System.out.println(conexion);
            System.out.println(protocolo +servidor + ":" + puerto +usuario+contrasena);
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "No se ha podido conectar con la Base de Datos");
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Error al leer el fichero de configuracion");
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Error al leer el fichero de configuracion");
        }catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "No se ha podido cargar el driver de la Base de Datos");
        }
    }

    public void crearBaseDatos() throws SQLException {
        System.out.println(conexion);

        String sentenciaCrear = "CREATE DATABASE aplicacion";
        PreparedStatement consulta1 = null;
        consulta1 = conexion.prepareStatement(sentenciaCrear);
        //consulta1.setString(1,baseDatos);
        consulta1.executeUpdate();


    }
    public void conectarNuevaBase(){
        try {
            configuracion.load(new FileInputStream("configuracionBBDD.props"));
             driver = configuracion.getProperty("driver");
             protocolo = configuracion.getProperty("protocolo");
             servidor = configuracion.getProperty("servidor");
             puerto = configuracion.getProperty("puerto");
             baseDatos = configuracion.getProperty("basedatos");
             usuario = configuracion.getProperty("usuario");
             contrasena = configuracion.getProperty("contrasena");

            Class.forName(driver).newInstance();
            conexion = DriverManager.getConnection(protocolo +servidor + ":" + puerto + "/" + baseDatos,usuario,contrasena);
            System.out.println(conexion);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se ha podido conectar con la Base de Datos");
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Error al leer el fichero de configuracion");
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Error al leer el fichero de configuracion");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "No se ha podido cargar el driver de la Base de Datos");
        }
    }
    public void crearTablas() throws SQLException {
        String sqlPabellon = "create table pabellon(\n" +
                "id int primary key auto_increment,\n" +
                "nombre varchar(40) unique not null,\n" +
                "localidad_pabellon varchar(40),\n" +
                "fecha_apertura date\n" +
                ");";
        String sqlEquipo = "create table equipo(\n" +
                "id int primary key auto_increment,\n" +
                "nombre varchar(40),\n" +
                "id_pabellon int unique,\n" +
                "localidad_equipo varchar(40),\n" +
                "foreign key (id_pabellon) references pabellon (id)\n" +
                ");";
        String sqlJugador = "create table jugador(\n" +
                "id int primary key auto_increment,\n" +
                "nombre varchar(40),\n" +
                "dorsal int,\n" +
                "fecha_nacimiento date,\n" +
                "id_equipo int,\n" +
                "foreign key (id_equipo) references equipo (id)\n" +
                ");";

        conexion.setAutoCommit(false);
        PreparedStatement sentenciaPabellon = null;
        sentenciaPabellon = conexion.prepareStatement(sqlPabellon);
        sentenciaPabellon.executeUpdate();

        PreparedStatement sentenciaEquipo = null;
        sentenciaEquipo = conexion.prepareStatement(sqlEquipo);
        sentenciaEquipo.executeUpdate();

        PreparedStatement sentenciaJugador = null;
        sentenciaJugador = conexion.prepareStatement(sqlJugador);
        sentenciaJugador.executeUpdate();

        conexion.commit();
    }

    public void crearProcedimientos() throws SQLException {
        String sqlAltaEquipo =
                "create procedure alta_equipo(pnombre varchar(40),pid_pabellon int, plocalidad_equipo varchar(40))\n" +
                "begin\n" +
                "if((select count(*) from equipo where id_pabellon=pid_pabellon))>0 then\n" +
                "select \"equipo con ese pabellon ya creado\";\n" +
                "else\n" +
                "insert into equipo(nombre,id_pabellon,localidad_equipo) values (pnombre,pid_pabellon,plocalidad_equipo);\n" +
                "end if;\n" +
                "end;";
        String sqlModificarEquipo =
                "create procedure modificar_equipo(pid int, pnombre varchar(40), pid_pabellon int, plocalidad_equipo varchar(40))\n" +
                "begin\n" +
                "if((select count(*) from equipo where id_pabellon=pid_pabellon))>0 then\n" +
                "select \"equipo con ese pabellon ya creado\";\n" +
                "else\n" +
                "update equipo set nombre = pnombre, id_pabellon= pid_pabellon, localidad_equipo = plocalidad_equipo where id = pid;\n" +
                "end if;\n" +
                "end;";
        String sqlBorrarEquipo =
                "create procedure borrar_equipo(pid_equipo int)\n" +
                "begin\n" +
                "if((select count(*) from jugador where id_equipo=pid_equipo))=0 then\n" +
                "delete from equipo where id = pid_equipo;\n" +
                "else\n" +
                "update jugador set id_equipo= null where id_equipo = pid_equipo;\n" +
                "delete from equipo where id = pid_equipo;\n" +
                "end if;\n" +
                "end;";
        String sqlAltaJugador =
                "create procedure alta_jugador(pnombre varchar(40),pdorsal varchar(40), pfecha_nacimiento date, pid_equipo int)\n" +
                "begin\n" +
                "if((select count(*) from jugador where id_equipo=pid_equipo and nombre = pnombre))>0 then\n" +
                "select \"jugador con ese equipo ya creado\";\n" +
                "else\n" +
                "insert into jugador(nombre,dorsal,fecha_nacimiento,id_equipo) values (pnombre,pdorsal,pfecha_nacimiento,pid_equipo);\n" +
                "end if;\n" +
                "end;";
        String sqlBorrarPabellon =
                "create procedure borrar_pabellon(pid_pabellon int)\n" +
                "begin\n" +
                "if((select count(*) from equipo where id_pabellon=pid_pabellon))=0 then\n" +
                "delete from pabellon where id = pid_pabellon;\n" +
                "else\n" +
                "update equipo set id_pabellon= null where id_pabellon = pid_pabellon;\n" +
                "delete from pabellon where id = pid_pabellon;\n" +
                "end if;\n" +
                "end;";
        String sqlMostrarBusqueda = "create procedure mostrar_campos(caracteres varchar(40))\n" +
                "begin\n" +
                "declare cadena varchar(40);\n" +
                "set cadena = caracteres;\n" +
                "if((select count(id) from pabellon where nombre like cadena))>=1 then\n" +
                "select * from pabellon where nombre like cadena;\n" +
                "else if((select count(id) from equipo where nombre like cadena))>=1 then\n" +
                "select * from equipo where nombre like cadena;\n" +
                "else if((select count(id) from jugador where nombre like cadena))>=1 then\n" +
                "select * from jugador where nombre like cadena;\n" +
                "end if;\n" +
                "end if;\n" +
                "end if;\n" +
                "end;";

        String sqlContarJugadoresEquipo = "create function contar_jugadores_equipo(pid int)\n" +
                "returns int\n" +
                "begin\n" +
                "return (select count(id) from jugador where id_equipo=pid);\n" +
                "end;";
        String sqlPrimerPabellonAbierto =
                "create function primer_pabellon_abierto()\n" +
                "returns varchar(50)\n" +
                "begin\n" +
                "return (select * from pabellon where fecha_apertura=(select min(fecha_apertura) from pabellon));\n" +
                "end;";

            conexion.setAutoCommit(false);
        PreparedStatement sentenciaAltaEquipo = null;
        sentenciaAltaEquipo = conexion.prepareStatement(sqlAltaEquipo);
        sentenciaAltaEquipo.executeUpdate();

        PreparedStatement sentenciaModificarEquipo = null;
        sentenciaModificarEquipo = conexion.prepareStatement(sqlModificarEquipo);
        sentenciaModificarEquipo.executeUpdate();

        PreparedStatement sentenciaEliminarEquipo = null;
        sentenciaEliminarEquipo = conexion.prepareStatement(sqlBorrarEquipo);
        sentenciaEliminarEquipo.executeUpdate();

        PreparedStatement sentenciaAltaJugador = null;
        sentenciaAltaJugador = conexion.prepareStatement(sqlAltaJugador);
        sentenciaAltaJugador.executeUpdate();

        PreparedStatement sentenciaEliminarPabellon = null;
        sentenciaEliminarPabellon = conexion.prepareStatement(sqlBorrarPabellon);
        sentenciaEliminarPabellon.executeUpdate();

        PreparedStatement sentenciaMostrarResultadosBusqueda= null;
        sentenciaMostrarResultadosBusqueda = conexion.prepareStatement(sqlMostrarBusqueda);
        sentenciaMostrarResultadosBusqueda.executeUpdate();

        PreparedStatement sentenciaContarJugadores = null;
        sentenciaContarJugadores = conexion.prepareStatement(sqlContarJugadoresEquipo);
        sentenciaContarJugadores.executeUpdate();

        PreparedStatement sentenciaPrimerPabellon = null;
        sentenciaPrimerPabellon = conexion.prepareStatement(sqlPrimerPabellonAbierto);
        sentenciaPrimerPabellon.executeUpdate();

        conexion.commit();

    }
    public void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * este metodo se encarga de anyadir un nuevo jugador a la tabla de jugadores
     * se creara a traves de los parametros recibidos
     *
     * @param nombre          de tipo String
     * @param dorsal          de tipo double
     * @param fechaNacimiento de tipo LocalDate
     */

    public void insertarJugador(String nombre, int dorsal, LocalDate fechaNacimiento, int idJugador) throws SQLException {
        String sentenciaSQL = "call alta_jugador(?,?,?,?)";
        CallableStatement procedimiento = null;

        procedimiento = conexion.prepareCall(sentenciaSQL);
        procedimiento.setString(1, nombre);
        procedimiento.setInt(2, dorsal);
        procedimiento.setDate(3, Date.valueOf(fechaNacimiento));
        procedimiento.setInt(4, idJugador);

        procedimiento.execute();
    }

    public ResultSet consultarJugadores() throws SQLException {
        /*String sentenciaSQL = "call mostrar_jugador()";
        CallableStatement procedimiento = null;
        ResultSet resultado = null;
        procedimiento = conexion.prepareCall(sentenciaSQL);
        resultado = procedimiento.executeQuery();
        */

        String consulta = "SELECT * FROM jugador";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement(consulta);
        resultado = sentencia.executeQuery();

        return resultado;
    }

    public String contarJugadoresEquipo(int valor) throws SQLException {

        String consulta = "SELECT contar_jugadores_equipo(?)";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1, valor);
        resultado = sentencia.executeQuery();
        resultado.next();

        String cadena = resultado.getString(1);

        return cadena;
    }

    public void modificarJugador(int id, String nombre, int dorsal, LocalDate fecha_nacimiento, int id_equipo) throws SQLException {
        String sentenciaSQL = "UPDATE JUGADOR SET nombre = ?, dorsal = ?, fecha_nacimiento = ?, id_equipo = ? WHERE id = ?";
        PreparedStatement actualizacion = null;

        actualizacion = conexion.prepareStatement(sentenciaSQL);
        actualizacion.setInt(5, id);
        actualizacion.setString(1, nombre);
        actualizacion.setInt(2, dorsal);
        actualizacion.setDate(3, Date.valueOf(fecha_nacimiento));
        actualizacion.setInt(4, id_equipo);

        actualizacion.executeUpdate();
    }

    public void eliminarJugador(int id) throws SQLException {
        String sentenciaSQL = "delete from jugador where id = ?";
        PreparedStatement consulta = null;

        consulta = conexion.prepareStatement(sentenciaSQL);
        consulta.setInt(1, id);

        consulta.executeUpdate();
    }

    public void insertarEquipo(String nombre, int pabellon, String localidad) throws SQLException {
        String sentenciaSQL = "call alta_equipo(?,?,?)";
        CallableStatement procedimiento = null;

        procedimiento = conexion.prepareCall(sentenciaSQL);
        procedimiento.setString(1, nombre);
        procedimiento.setInt(2, pabellon);
        procedimiento.setString(3, localidad);

        procedimiento.execute();

    }

    public void modificarEquipo(int id, String nombre, int pabellon, String localidad) throws SQLException {
        String sentenciaSQL = "UPDATE equipo SET nombre = ?, id_pabellon = ?, localidad_equipo = ?WHERE id = ?";
        PreparedStatement actualizacion = null;

        actualizacion = conexion.prepareStatement(sentenciaSQL);
        actualizacion.setString(1, nombre);
        actualizacion.setInt(2, pabellon);
        actualizacion.setString(3, localidad);
        actualizacion.setInt(4, id);

        actualizacion.executeUpdate();

    }

    public void eliminarEquipo(int id) throws SQLException {
        String sentenciaSQL = "call borrar_equipo(?)";
        CallableStatement procedimiento = null;

        procedimiento = conexion.prepareCall(sentenciaSQL);
        procedimiento.setInt(1, id);

        procedimiento.execute();

    }

    public ResultSet consultarEquipos() throws SQLException {
        String consulta = "SELECT * FROM equipo";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement(consulta);
        resultado = sentencia.executeQuery();
        return resultado;
    }


    /**
     * este metodo se encarga de dar de alta un pabellon con los parametros recibidos
     *
     * @param nombre         de tipo String
     * @param localidad      de tipo String
     * @param fecha_apertura de tipo LocalDate
     */

    public void insertarPabellon(String nombre, String localidad, LocalDate fecha_apertura) throws SQLException {

        String sentenciaSql = "INSERT INTO PABELLON (nombre,localidad_pabellon,fecha_apertura) values (?,?,?)";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1, nombre);
        sentencia.setString(2, localidad);
        sentencia.setDate(3, Date.valueOf(fecha_apertura));
        sentencia.executeUpdate();
        System.out.println(conexion);
        if(sentencia == null){
            sentencia.close();
        }

    }
    public String consultarUltimoPabellon() throws  SQLException{
        String consulta = "select primer_pabellon_abierto()";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement(consulta);
        resultado = sentencia.executeQuery();
        resultado.next();
        String cadena =resultado.getString(1);
        return cadena;
    }
    public ResultSet consultarPabellones() throws SQLException {
        String consulta = "SELECT * FROM pabellon";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement(consulta);
        resultado = sentencia.executeQuery();


        return resultado;
    }

    public void modificarPabellon(int pid, String pnombre, String localidad, LocalDate pfechaApertura) throws SQLException {
        String sentenciaSQL = "UPDATE PABELLON SET nombre='" + pnombre + "',localidad_pabellon='" + localidad + "',fecha_apertura='" + pfechaApertura + "' WHERE id ='" + pid + "'";
        PreparedStatement actualizacion = null;

        actualizacion = conexion.prepareStatement(sentenciaSQL);
        actualizacion.executeUpdate();
    }

    public void eliminarPabellon(int id) throws SQLException {
        String sentenciaSQL = "call borrar_pabellon(?)";
        CallableStatement procedimineto = null;

        procedimineto = conexion.prepareCall(sentenciaSQL);
        procedimineto.setInt(1, id);
        procedimineto.execute();
    }

    public ResultSet mostrarBusqueda(String palabra) throws SQLException {
        String sentenciaSQL = "call mostrar_campos(?)";
        CallableStatement procedimiento = null;
        ResultSet resultado = null;

        procedimiento = conexion.prepareCall(sentenciaSQL);
        procedimiento.setString(1, palabra + '%');
        resultado = procedimiento.executeQuery();
        System.out.println(resultado);
        System.out.println(resultado.getFetchSize());
        return resultado;

    }


    /**
     * este metodo se encarga de recoger los objetos Pabellon que contiene la aplicacion
     * guardando los valores de sus campos en un archivo xml,
     * que es elegido o creado por el usuario
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarPabellonXML() throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        // creo un documento de extension xml
        Document documento = dom.createDocument(null, "xml", null);
        // creo el primer nodo del fichero XML
        // corresponde a la etiqueta "Pabellones"
        Element raiz = documento.createElement("Pabellones");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoPabellon;
        Element nodoDatos;
        Text dato;
        ResultSet resultado = null;
        // recorro la lista de pabellones y creo un subnodo por cada pabellon
        try {
            resultado = consultarPabellones();
            while (resultado.next()) {
                nodoPabellon = documento.createElement("Pabellon");
                raiz.appendChild(nodoPabellon);

                nodoDatos = documento.createElement("Id");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode("" + resultado.getInt(1));
                nodoDatos.appendChild(dato);
                // por cada dato de cada pabellon creo otro subnodo
                nodoDatos = documento.createElement("Nombre");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode(resultado.getString(2));
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("Localidad_Pabellon");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode(resultado.getString(3));
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("Fecha_Apertura");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode(String.valueOf(resultado.getDate(4)));
                nodoDatos.appendChild(dato);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // finalizo el documento xml y lo guardo en el fichero
        Source src = new DOMSource(documento);
        Result resultados = new StreamResult(new File("pabellones.xml"));
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(src, resultados);
    }

    public void exportarEquipoXML() throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        // creo un documento de extension xml
        Document documento = dom.createDocument(null, "xml", null);
        // creo el primer nodo del fichero XML
        // corresponde a la etiqueta "Pabellones"
        Element raiz = documento.createElement("Equipos");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoPabellon;
        Element nodoDatos;
        Text dato;
        ResultSet resultado = null;
        // recorro la lista de pabellones y creo un subnodo por cada pabellon
        try {
            resultado = consultarEquipos();
            while (resultado.next()) {
                nodoPabellon = documento.createElement("Equipo");
                raiz.appendChild(nodoPabellon);

                nodoDatos = documento.createElement("Id");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode("" + resultado.getInt(1));
                nodoDatos.appendChild(dato);
                // por cada dato de cada pabellon creo otro subnodo
                nodoDatos = documento.createElement("Nombre");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode(resultado.getString(2));
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("Id_Pabellon");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode(""+resultado.getInt(3));
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("Localidad");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode(resultado.getString(4));
                nodoDatos.appendChild(dato);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // finalizo el documento xml y lo guardo en el fichero
        Source src = new DOMSource(documento);
        Result resultados = new StreamResult(new File("equipos.xml"));
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(src, resultados);
    }

    public void exportarJugadorXML() throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        // creo un documento de extension xml
        Document documento = dom.createDocument(null, "xml", null);
        // creo el primer nodo del fichero XML
        // corresponde a la etiqueta "Pabellones"
        Element raiz = documento.createElement("Jugadores");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoPabellon;
        Element nodoDatos;
        Text dato;
        ResultSet resultado = null;
        // recorro la lista de pabellones y creo un subnodo por cada pabellon
        try {
            resultado = consultarJugadores();
            while (resultado.next()) {
                nodoPabellon = documento.createElement("Jugador");
                raiz.appendChild(nodoPabellon);

                nodoDatos = documento.createElement("Id");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode("" + resultado.getInt(1));
                nodoDatos.appendChild(dato);
                // por cada dato de cada pabellon creo otro subnodo
                nodoDatos = documento.createElement("Nombre");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode(resultado.getString(2));
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("Dorsal");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode(""+resultado.getInt(3));
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("Fecha_Nacimiento");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode(String.valueOf(resultado.getDate(4)));
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("Id_Equipo");
                nodoPabellon.appendChild(nodoDatos);
                dato = documento.createTextNode(""+resultado.getInt(5));
                nodoDatos.appendChild(dato);
         }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // finalizo el documento xml y lo guardo en el fichero
        Source src = new DOMSource(documento);
        Result resultados = new StreamResult(new File("jugadores.xml"));
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(src, resultados);
    }
}












