package principal;

import javax.swing.*;

public class Util {
	/**
	 * creo un mensaje de error en el login cuando faltan datos
	 * recibiendo como parametro el contenido que quiero mostrar
	 * @param mensaje de tipo String
	 */
	 public static void mensajeLoginFaltaDatos(String mensaje){
	        JOptionPane.showMessageDialog(null,mensaje,"Error de Acceso",JOptionPane.ERROR_MESSAGE);
	    }
	/**
	 * creo un mensaje de error en el login cuando se produce un error al acceder
	 * recibiendo como parametro el contenido que quiero mostrar
	 * @param mensaje de tipo String
	 */
	 public static void mensajeLoginError(String mensaje){
	        JOptionPane.showMessageDialog(null,mensaje,"Error de Acceso",JOptionPane.ERROR_MESSAGE);
	    }

	/**
	 * creo un mensaje de error en la pestanya cuando sucede un error al anyadir
	 * un jugador al array, recibiendo como parametro el contenido que quiero mostrar
	 * @param mensaje de tipo String
	 */
	 public static void mensajeAnyadirError(String mensaje){
			JOptionPane.showMessageDialog(null,mensaje,"Error al Añadir",JOptionPane.ERROR_MESSAGE);
		}
}
