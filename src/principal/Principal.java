package principal;

import MVC.Controlador;
import MVC.Modelo;
import MVC.Vista;
import MVC.VistaLogin;
import usuarios.ListaUsuarios;


public class Principal {
    private int contador =0;

    public static void main(String[] args) {
        ListaUsuarios unaListaUsuarios = new ListaUsuarios();
        Util util = new Util();
        VistaLogin unaVistaLogin = new VistaLogin(unaListaUsuarios,util);
        Vista unaVista = new Vista(unaVistaLogin);


        Modelo unModelo = new Modelo();

        Controlador unControlador = new Controlador(unaVista, unModelo,unaVistaLogin,util);

    }
}
